function calculate(){
	var osnovica = parseInt(document.getElementById('osnovica').value);
	var km = parseInt(document.getElementById('km').value);
	var hours = document.getElementById('hours').selectedIndex;
	var dan = document.getElementById('day').selectedIndex;
	var uvecanje1 = 0; 
	if(km < 10 && km >= 0){
		uvecanje1 = 0.1;
	}
	else if(km >= 10 && km <= 30){
		uvecanje1 = km / 100;
	}
	else{
		uvecanje1 = 0.3;
	}
	
	uvecanje2 = 0;
	if(hours == 0){
		uvecanje2 = 0.2;
	}
	else if(hours == 1){
		uvecanje2 = 0.3;
	}
	else if(hours == 2){
		uvecanje2 = 0.5;
	}
	if(dan == 0){
		uvecanje2 *= 2;
	}
	osnovica *= (uvecanje1 + uvecanje2);
	document.getElementById('result').innerHTML = "Uvecanje zbog udaljenosti: " + uvecanje1*100 +"%<br/>" + "Uvecanje zbog termina: " + uvecanje2*100 +"%<br/><br/>"+"Zadovoljstvo ce vas kostati " + osnovica + "€";
}
